package com.jcmendonca.transportadora.controle.carregamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControleCarregamentosApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControleCarregamentosApplication.class, args);
    }

}

