package com.jcmendonca.transportadora.controle.carregamentos.controller.impl;

import com.jcmendonca.transportadora.controle.carregamentos.controller.SolicitacaoTransporteController;
import com.jcmendonca.transportadora.controle.carregamentos.dto.SolicitacaoTransporteDTO;
import com.jcmendonca.transportadora.controle.carregamentos.model.SolicitacaoTransporte;
import com.jcmendonca.transportadora.controle.carregamentos.repository.SolicitacaoTransporteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("solicitacao-transporte")
public class SolicitacaoTransporteControllerImpl implements SolicitacaoTransporteController {

    @Autowired
    private SolicitacaoTransporteRepository repository;

    @RequestMapping
    @PostMapping
    @Override
    public SolicitacaoTransporteDTO registrarSolicitacaoTransporte(SolicitacaoTransporteDTO solicitacaoTransporteDTO) {

        SolicitacaoTransporte solicitacaoTransporte = new SolicitacaoTransporte();

        return null;
    }

    //@RequestMapping("/{start}/{end}")

    public List<SolicitacaoTransporteDTO> obterTodasSolicitacoesPeriodo(@PathVariable("start") LocalDateTime start, @PathVariable("end") LocalDateTime end) {
        return null;
    }


    @GetMapping
    @Override
    public List<SolicitacaoTransporteDTO> obterTodasSolicitacoesPeriodo() {
        List<SolicitacaoTransporte> solicitacoes = (List<SolicitacaoTransporte>) repository.findAll();
        return solicitacoes.stream().map(solicitacao -> {
            SolicitacaoTransporteDTO dto = new SolicitacaoTransporteDTO();
            dto.id = solicitacao.getId();
            dto.solicitante = solicitacao.getSolicitante();
            dto.dataHoraSolicitacao = solicitacao.getDataHoraSolicitacao();
            dto.descricao = solicitacao.getDescricao();
            dto.origem = solicitacao.getOrigem();
            dto.destino = solicitacao.getDestino();
            dto.fragil = solicitacao.isFragil();
            dto.prazoEntrega = solicitacao.getPrazoEntrega();
            dto.altura = solicitacao.getAltura();
            dto.largura = solicitacao.getLargura();
            dto.peso = solicitacao.getPeso();
            dto.profundidade = solicitacao.getProfundidade();
            return dto;
        }).collect(Collectors.toList());
    }
}
