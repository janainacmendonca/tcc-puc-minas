package com.jcmendonca.transportadora.controle.carregamentos.repository;

import com.jcmendonca.transportadora.controle.carregamentos.model.SolicitacaoTransporte;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "solicitacao", path = "solicitacao")
public interface SolicitacaoTransporteRepository extends PagingAndSortingRepository<SolicitacaoTransporte, Long> {

    List<SolicitacaoTransporte> findAllByDataHoraSolicitacaoBetween(
            @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") @Param("start") LocalDateTime start,
            @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") @Param("end") LocalDateTime end);
}
