CREATE TABLE solicitacao_transporte (
    id INT PRIMARY KEY,
    solicitante VARCHAR(255),
    descricao VARCHAR(1000),
    origem VARCHAR(50),
    destino VARCHAR(50),
    altura INT,
    largura INT,
    profundidade INT,
    peso INT,
    fragil BOOLEAN,
    data_hora_solicitacao TIMESTAMP,
    prazo_entrega TIMESTAMP
);

create sequence hibernate_sequence;