import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';

import { CarregamentosComponent } from './carregamentos/carregamentos.component';
import { HomeComponent } from './home/home.component';
import { ExpedicaoComponent } from './expedicao/expedicao.component';

import { CarregamentosService } from './carregamentos/carregamentos.service';

@NgModule({
  declarations: [
    AppComponent,
    CarregamentosComponent,
    HomeComponent,
    ExpedicaoComponent
  ],
  imports: [
    BrowserModule,
    UiModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CarregamentosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
