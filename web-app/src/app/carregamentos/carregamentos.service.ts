import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarregamentosService {

  private urlSolicitacoes = '/controle-carregamentos/solicitacao';

  constructor(
    private http: HttpClient
  ) { }

  getSolicitacoesTransporte(): Observable<any> {
    return this.http.get<any>(environment.baseUrl + this.urlSolicitacoes);
  }
}
