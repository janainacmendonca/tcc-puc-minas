import { TestBed } from '@angular/core/testing';

import { CarregamentosService } from './carregamentos.service';

describe('CarregamentosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarregamentosService = TestBed.get(CarregamentosService);
    expect(service).toBeTruthy();
  });
});
