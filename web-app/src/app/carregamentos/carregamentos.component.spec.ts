import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarregamentosComponent } from './carregamentos.component';

describe('CarregamentosComponent', () => {
  let component: CarregamentosComponent;
  let fixture: ComponentFixture<CarregamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarregamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarregamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
