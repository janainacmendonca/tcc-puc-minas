import { Component, OnInit } from '@angular/core';

import { CarregamentosService } from './carregamentos.service';

@Component({
  selector: 'app-carregamentos',
  templateUrl: './carregamentos.component.html',
  styleUrls: ['./carregamentos.component.css']
})
export class CarregamentosComponent implements OnInit {

  solicitacoesTransporte: any;

  constructor(
    private carregamentosService: CarregamentosService
  ) { }

  ngOnInit() {
    this.carregamentosService.getSolicitacoesTransporte().subscribe(response => this.solicitacoesTransporte = response._embedded.solicitacao);
  }
}
