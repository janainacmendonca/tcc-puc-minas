import { Component, OnInit } from '@angular/core';

import { ExpedicaoService } from './expedicao.service';

@Component({
  selector: 'app-expedicao',
  templateUrl: './expedicao.component.html',
  styleUrls: ['./expedicao.component.css']
})
export class ExpedicaoComponent implements OnInit {

  itensExpedicao: any;
  filtroPeriodo: any;

  constructor(
    private expedicaoService: ExpedicaoService
  ) { }

  ngOnInit() {
   //this.expedicaoService.getItensExpedicao().subscribe(response => this.itensExpedicao = response);
    this.filtroPeriodo = "todos";
    this.filtrar();
  }

  filtrar() {
    console.log(this.filtroPeriodo);

    var date = new Date();

    switch (this.filtroPeriodo) {
      case "todos":
        date = undefined;
        break;

      case "proximos7dias":
        date = this.addDays(date, 7);
        break;

      case "proximos15dias":
        date = this.addDays(date, 15);
        break;

      case "proximos30dias":
        date = this.addDays(date, 30);
        break;
    }
    this.expedicaoService.getItensExpedicaoFiltro(date).subscribe(response => this.itensExpedicao = response);

  }

  addDays(date, days) {
    var date2 = new Date(date.valueOf());
    date2.setDate(date2.getDate() + days);
    return date2;
  }



}
