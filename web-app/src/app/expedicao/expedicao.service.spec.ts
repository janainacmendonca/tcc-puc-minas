import { TestBed } from '@angular/core/testing';

import { ExpedicaoService } from './expedicao.service';

describe('ExpedicaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpedicaoService = TestBed.get(ExpedicaoService);
    expect(service).toBeTruthy();
  });
});
