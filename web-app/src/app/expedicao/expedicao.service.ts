import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {environment} from '../../environments/environment';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ExpedicaoService {

  private expedicaoPath = '/controle-expedicao/expedicao/itens?apenasAgendados=true';

  private expedicaoFiltroPath = '/controle-expedicao/expedicao/itens/agendados/';

  constructor(
    private http: HttpClient
  ) {}

  getItensExpedicao(): Observable<any> {
    return this.http.get<any>(environment.baseUrl + this.expedicaoPath);
  }

  getItensExpedicaoFiltro(dataFinal): Observable<any> {
    if(!dataFinal) {
      dataFinal = new Date('2050-12-31');
    }
    var hoje = new Date()
    return this.http.get<any>(environment.baseUrl + this.expedicaoFiltroPath + '?dataInicial=' + this.formatDate(hoje) + '&dataFinal=' + this.formatDate(dataFinal));
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
}
