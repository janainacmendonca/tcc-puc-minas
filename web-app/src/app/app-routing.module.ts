import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CarregamentosComponent } from './carregamentos/carregamentos.component';
import { ExpedicaoComponent } from './expedicao/expedicao.component';

const routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'carregamentos', component: CarregamentosComponent },
  { path: 'expedicao', component: ExpedicaoComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
