# POC - Sistema de controle de logística

Prova de conceito do protótipo arquitetural desenvolvido no trabalho de conlusão de curso da especialização
em arquitetura de software distribuído da PUC Minas.

Aluna: Janaína Carraro Mendonça Lima

Contexto do projeto: Sistema de controle de logística

URL do vídeo de apresentação: https://www.youtube.com/watch?v=5NvUKzlQfo8&feature=youtu.be

Como executar a POC:

1. Clonar este repositório por meio do `git clone`
2. Iniciar o eureka-server:

	`cd eureka-server`
	
	`mvn spring-boot:run`
	
3. Iniciar o zuul-server:

	`cd zuul-server`
	
	`mvn spring-boot:run`
	
4. Iniciar o micro serviço de cada módulo:

	`cd controle-carregamentos`
	
	`mvn spring-boot:run`
	
	
	
	`cd controle-expedicao`
	
	`mvn spring-boot:run`
	
	
	
	`cd controle-tabelas-frete`
	
	`mvn spring-boot:run`
	
5. Iniciar o frontend da aplicação:
	`cd web-app`
	
	`ng-serve open`
	
Pré requisitos:
Banco de dados PostgreSQL com as bases de dados para os serviços (ver os scripts de criação das tabelas na pasta 
/src/resources/ de cada micro serviço 

Se desejar, importe a coleção disponibilizada [aqui](https://bitbucket.org/janainacmendonca/tcc-puc-minas/src/1c4ed99c2266/postman-collection.json)
no Postman para chamar os endpoints das APIs dos micro serviços.