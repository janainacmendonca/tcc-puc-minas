package com.jcmendonca.transportadora.controletabelasfrete.api;

import com.jcmendonca.transportadora.controletabelasfrete.dto.RealizarSimulacaoDTO;
import com.jcmendonca.transportadora.controletabelasfrete.dto.ResultadoSimulacaoDTO;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("tabelas-frete")
public interface TabelasFreteController {

    @RequestMapping("/simulacao")
    ResultadoSimulacaoDTO realizarSimulacao(RealizarSimulacaoDTO dadosSimulacao);

}
