package com.jcmendonca.transportadora.controletabelasfrete.dto;

import java.time.LocalDateTime;

public class RealizarSimulacaoDTO {

    public LocalDateTime prazoEntrega;

    public String descricao;

    public String origem;

    public String destino;

    public Integer largura;

    public Integer altura;

    public Integer profundidade;

    public Integer peso;

    public boolean fragil;
}
