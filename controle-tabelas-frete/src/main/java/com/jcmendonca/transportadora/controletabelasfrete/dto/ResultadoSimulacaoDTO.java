package com.jcmendonca.transportadora.controletabelasfrete.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ResultadoSimulacaoDTO {

    public BigDecimal valorTotal;

    public LocalDateTime estimativaEntrega;
}
