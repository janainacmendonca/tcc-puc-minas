package com.jcmendonca.transportadora.controletabelasfrete.service;

import com.jcmendonca.transportadora.controletabelasfrete.api.TabelasFreteController;
import com.jcmendonca.transportadora.controletabelasfrete.dto.RealizarSimulacaoDTO;
import com.jcmendonca.transportadora.controletabelasfrete.dto.ResultadoSimulacaoDTO;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@RestController
public class TabelasFreteControllerImpl implements TabelasFreteController {

    @Override
    public ResultadoSimulacaoDTO realizarSimulacao(RealizarSimulacaoDTO dadosSimulacao) {

        ResultadoSimulacaoDTO resultado = new ResultadoSimulacaoDTO();
        resultado.estimativaEntrega = LocalDateTime.now().plusDays(3);
        resultado.valorTotal = new BigDecimal("120.45");

        return resultado;
    }

}
