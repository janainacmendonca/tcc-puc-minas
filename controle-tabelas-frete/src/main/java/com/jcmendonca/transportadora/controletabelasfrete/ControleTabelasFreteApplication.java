package com.jcmendonca.transportadora.controletabelasfrete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ControleTabelasFreteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControleTabelasFreteApplication.class, args);
    }

}

