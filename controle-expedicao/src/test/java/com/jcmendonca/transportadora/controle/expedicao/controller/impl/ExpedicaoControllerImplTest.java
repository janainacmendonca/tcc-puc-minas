package com.jcmendonca.transportadora.controle.expedicao.controller.impl;

import com.jcmendonca.transportadora.controle.expedicao.ItemExpedicaoRepository;
import com.jcmendonca.transportadora.controle.expedicao.dto.ItemExpedicaoDTO;
import com.jcmendonca.transportadora.controle.expedicao.integracao.ControleCarregamentosClient;
import com.jcmendonca.transportadora.controle.expedicao.integracao.SolicitacaoTransporteDTO;
import com.jcmendonca.transportadora.controle.expedicao.model.ItemExpedicao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ExpedicaoControllerImplTest {

    @Mock
    private ItemExpedicaoRepository itemExpedicaoRepository;

    @Mock
    private ControleCarregamentosClient controleCarregamentosClient;

    @InjectMocks
    private ExpedicaoControllerImpl classToTest;

    @Test
    public void obterItensAgendados() {
        String dataInicial = "2019-02-10";
        String dataFinal = "2019-02-15";

        ItemExpedicao itemExpedicao1 = criarItemExpedicao(144, 124, LocalDate.parse("2019-02-10"));
        ItemExpedicao itemExpedicao2 = criarItemExpedicao(214, 123, LocalDate.parse("2019-02-14"));

        Mockito.when(itemExpedicaoRepository.findAllByDataExpedicaoBetweenOrderByDataExpedicao(LocalDate.parse(dataInicial), LocalDate.parse(dataFinal)))
               .thenReturn(Arrays.asList(itemExpedicao1, itemExpedicao2));

        SolicitacaoTransporteDTO solicitacao1 = new SolicitacaoTransporteDTO();
        Mockito.when(controleCarregamentosClient.obterSolicitacaoPeloId(124)).thenReturn(solicitacao1);

        SolicitacaoTransporteDTO solicitacao2 = new SolicitacaoTransporteDTO();
        Mockito.when(controleCarregamentosClient.obterSolicitacaoPeloId(123)).thenReturn(solicitacao2);

        List<ItemExpedicaoDTO> itens = classToTest.obterItensAgendados(dataInicial, dataFinal);

        assertThat(itens).hasSize(2);
        assertThat(itens.get(0).id).isEqualTo(144);
        assertThat(itens.get(0).idSolicitacaoTransporte).isEqualTo(124);
        assertThat(itens.get(0).dataExpedicao).isEqualTo(LocalDate.parse("2019-02-10"));
        assertThat(itens.get(0).detalheSolicitacao).isEqualTo(solicitacao1);

        assertThat(itens.get(1).id).isEqualTo(214);
        assertThat(itens.get(1).idSolicitacaoTransporte).isEqualTo(123);
        assertThat(itens.get(1).dataExpedicao).isEqualTo(LocalDate.parse("2019-02-14"));
        assertThat(itens.get(1).detalheSolicitacao).isEqualTo(solicitacao2);

        Mockito.verify(controleCarregamentosClient).obterSolicitacaoPeloId(123);
        Mockito.verify(controleCarregamentosClient).obterSolicitacaoPeloId(124);
    }

    @Test
    public void definirItemExpedicao() {
        ItemExpedicaoDTO item = new ItemExpedicaoDTO();
        item.idSolicitacaoTransporte = 124;
        item.dataExpedicao = LocalDate.now().plusDays(1);

        classToTest.definirItemExpedicao(item);

        ArgumentCaptor<ItemExpedicao> argumentCaptor = ArgumentCaptor.forClass(ItemExpedicao.class);
        Mockito.verify(itemExpedicaoRepository).save(argumentCaptor.capture());

        ItemExpedicao itemExpedicao = argumentCaptor.getValue();
        assertThat(itemExpedicao.getIdSolicitacaoTransporte()).isEqualTo(item.idSolicitacaoTransporte);
        assertThat(itemExpedicao.getDataExpedicao()).isEqualTo(item.dataExpedicao);
    }

    private ItemExpedicao criarItemExpedicao(int id, int idSolicitacaoTransporte, LocalDate dataExpedicao) {
        ItemExpedicao itemExpedicao = new ItemExpedicao();
        itemExpedicao.setIdSolicitacaoTransporte(idSolicitacaoTransporte);
        itemExpedicao.setId(id);
        itemExpedicao.setDataExpedicao(dataExpedicao);
        return itemExpedicao;
    }


}