CREATE TABLE item_expedicao (
    id INT PRIMARY KEY,
    id_solicitacao_transporte INT,
    data_expedicao DATE
);

create sequence hibernate_sequence;