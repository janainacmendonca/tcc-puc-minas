package com.jcmendonca.transportadora.controle.expedicao.integracao;

import java.time.LocalDateTime;

public class SolicitacaoTransporteDTO {

    public int id;

    public LocalDateTime dataHoraSolicitacao;

    public String solicitante;

    public LocalDateTime prazoEntrega;

    public String descricao;

    public String origem;

    public String destino;

    public int largura;

    public int altura;

    public int profundidade;

    public int peso;

    public boolean fragil;
}
