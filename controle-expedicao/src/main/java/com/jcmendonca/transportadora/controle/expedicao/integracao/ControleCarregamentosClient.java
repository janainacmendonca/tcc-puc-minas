package com.jcmendonca.transportadora.controle.expedicao.integracao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("controle-carregamentos")
public interface ControleCarregamentosClient {

    @RequestMapping("/solicitacao-transporte")
    SolicitacaoTransporteDTO registrarSolicitacaoTransporte(SolicitacaoTransporteDTO dto);

    @RequestMapping("/solicitacao-transporte")
    List<SolicitacaoTransporteDTO> obterTodasSolicitacoesPeriodo();

    @RequestMapping("/solicitacao/{id}")
    SolicitacaoTransporteDTO obterSolicitacaoPeloId(@PathVariable("id") int idSolicitacaoTransporte);
}
