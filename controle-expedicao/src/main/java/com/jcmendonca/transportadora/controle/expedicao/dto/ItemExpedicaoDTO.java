package com.jcmendonca.transportadora.controle.expedicao.dto;

import com.jcmendonca.transportadora.controle.expedicao.integracao.SolicitacaoTransporteDTO;

import java.time.LocalDate;

public class ItemExpedicaoDTO {

    public int id;

    public int idSolicitacaoTransporte;

    public LocalDate dataExpedicao;

    public SolicitacaoTransporteDTO detalheSolicitacao;
}
