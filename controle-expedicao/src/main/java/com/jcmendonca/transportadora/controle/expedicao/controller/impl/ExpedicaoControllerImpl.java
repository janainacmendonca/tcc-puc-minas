package com.jcmendonca.transportadora.controle.expedicao.controller.impl;

import com.jcmendonca.transportadora.controle.expedicao.ItemExpedicaoRepository;
import com.jcmendonca.transportadora.controle.expedicao.controller.ExpedicaoController;
import com.jcmendonca.transportadora.controle.expedicao.dto.ItemExpedicaoDTO;
import com.jcmendonca.transportadora.controle.expedicao.integracao.ControleCarregamentosClient;
import com.jcmendonca.transportadora.controle.expedicao.integracao.SolicitacaoTransporteDTO;
import com.jcmendonca.transportadora.controle.expedicao.model.ItemExpedicao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ExpedicaoControllerImpl implements ExpedicaoController {

    @Autowired
    private ItemExpedicaoRepository itemExpedicaoRepository;

    @Autowired
    private ControleCarregamentosClient controleCarregamentosClient;

    @Override
    public List<ItemExpedicaoDTO> obterItensExpedicao(boolean apenasAgendados) {
        List<ItemExpedicaoDTO> result = new ArrayList<>();
        Iterable<ItemExpedicao> items = itemExpedicaoRepository.findAll();
        items.forEach(item -> {
            ItemExpedicaoDTO dto = toItemExpedicaoDTO(item);
            result.add(dto);
        });

        if (!apenasAgendados) {
            List<SolicitacaoTransporteDTO> solicitacoes = controleCarregamentosClient
                    .obterTodasSolicitacoesPeriodo();
            solicitacoes.forEach(solicitacao -> {
                ItemExpedicaoDTO dto = new ItemExpedicaoDTO();
                dto.id = 0;
                dto.dataExpedicao = null;
                dto.idSolicitacaoTransporte = solicitacao.id;
                dto.detalheSolicitacao = solicitacao;
                result.add(dto);
            });
        }

        return result;
    }

    private ItemExpedicaoDTO toItemExpedicaoDTO(ItemExpedicao item) {
        ItemExpedicaoDTO dto = new ItemExpedicaoDTO();
        dto.id = item.getId();
        dto.dataExpedicao = item.getDataExpedicao();
        dto.idSolicitacaoTransporte = item.getIdSolicitacaoTransporte();
        dto.detalheSolicitacao = controleCarregamentosClient.obterSolicitacaoPeloId(item.getIdSolicitacaoTransporte());
        return dto;
    }

    @Override
    public List<ItemExpedicaoDTO> obterItensAgendados(String dataInicial, String dataFinal) {
        List<ItemExpedicao> items = itemExpedicaoRepository.findAllByDataExpedicaoBetweenOrderByDataExpedicao(LocalDate.parse(dataInicial), LocalDate.parse(dataFinal));
        return items.stream().map(i -> toItemExpedicaoDTO(i)).collect(Collectors.toList());
    }

    @Override
    public void definirItemExpedicao(ItemExpedicaoDTO dto) {
        ItemExpedicao itemExpedicao = new ItemExpedicao();
        itemExpedicao.setDataExpedicao(dto.dataExpedicao);
        itemExpedicao.setIdSolicitacaoTransporte(dto.idSolicitacaoTransporte);

        itemExpedicaoRepository.save(itemExpedicao);
    }
}
