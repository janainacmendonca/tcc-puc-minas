package com.jcmendonca.transportadora.controle.expedicao;

import com.jcmendonca.transportadora.controle.expedicao.model.ItemExpedicao;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "item", path = "item")
public interface ItemExpedicaoRepository extends PagingAndSortingRepository<ItemExpedicao, Integer> {

    List<ItemExpedicao> findAllByDataExpedicaoBetweenOrderByDataExpedicao(LocalDate dataInicial, LocalDate dataFinal);
}
