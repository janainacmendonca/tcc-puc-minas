package com.jcmendonca.transportadora.controle.expedicao.controller;

import com.jcmendonca.transportadora.controle.expedicao.dto.ItemExpedicaoDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@RequestMapping("expedicao")
public interface ExpedicaoController {

    @RequestMapping("/itens")
    List<ItemExpedicaoDTO> obterItensExpedicao(@RequestParam("apenasAgendados") boolean apenasAgendados);

    @RequestMapping("/itens/agendados")
    List<ItemExpedicaoDTO> obterItensAgendados(@RequestParam("dataInicial") String dataInicial,
                                               @RequestParam("dataFinal") String dataFinal);

    @RequestMapping("/definir")
    void definirItemExpedicao(ItemExpedicaoDTO dto);

}
