package com.jcmendonca.transportadora.controle.expedicao.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class ItemExpedicao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int idSolicitacaoTransporte;

    private LocalDate dataExpedicao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSolicitacaoTransporte() {
        return idSolicitacaoTransporte;
    }

    public void setIdSolicitacaoTransporte(int idSolicitacaoTransporte) {
        this.idSolicitacaoTransporte = idSolicitacaoTransporte;
    }

    public LocalDate getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(LocalDate dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }
}
