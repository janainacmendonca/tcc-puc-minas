package com.jcmendonca.transportadora.controle.expedicao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ControleExpedicaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControleExpedicaoApplication.class, args);
    }

}

