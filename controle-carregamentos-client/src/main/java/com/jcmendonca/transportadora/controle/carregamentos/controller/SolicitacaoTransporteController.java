package com.jcmendonca.transportadora.controle.carregamentos.controller;

import com.jcmendonca.transportadora.controle.carregamentos.dto.SolicitacaoTransporteDTO;

import java.time.LocalDateTime;
import java.util.List;

public interface SolicitacaoTransporteController {

    SolicitacaoTransporteDTO registrarSolicitacaoTransporte(SolicitacaoTransporteDTO dto);

    List<SolicitacaoTransporteDTO> obterTodasSolicitacoesPeriodo(LocalDateTime start, LocalDateTime end);

    List<SolicitacaoTransporteDTO> obterTodasSolicitacoesPeriodo();
}
