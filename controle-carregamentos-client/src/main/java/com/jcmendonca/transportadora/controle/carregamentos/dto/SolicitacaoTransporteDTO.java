package com.jcmendonca.transportadora.controle.carregamentos.dto;

import java.time.LocalDateTime;

public class SolicitacaoTransporteDTO {

    public long id;

    public LocalDateTime dataHoraSolicitacao;

    public String solicitante;

    public LocalDateTime prazoEntrega;

    public String descricao;

    public String origem;

    public String destino;

    public int largura;

    public int altura;

    public int profundidade;

    public int peso;

    public boolean fragil;
}
